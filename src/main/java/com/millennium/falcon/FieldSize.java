package com.millennium.falcon;

import java.lang.reflect.Field;

public interface FieldSize {
    long getSize(Object object, Field objectFiled);
}
