package com.millennium.falcon;


public interface ObjectSize {
    long getSize(Object object);

    long getSize(Object object, boolean deep);
}
