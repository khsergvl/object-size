package com.millennium.falcon.impl.support;

import com.millennium.falcon.FieldSize;
import com.millennium.falcon.utils.ReflectionSupport;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

@Slf4j
public class ArrayFieldSize implements FieldSize {

    @Override
    public long getSize(Object object, Field objectFiled) {
        long size = 12;
        Object[] value = buildArrayForCommonLogic(object, objectFiled);
        if (value != null) {
            for (int i = 0; i < value.length; i++) {
                Object arrayElement = Array.get(value, i);
                size = size + getArrayElementSize(arrayElement);
            }
        }
        return size;
    }

    /**
     * This method simplify recursive
     * getSize method call
     *
     * @param object, objectFiled
     *                for common reflective array extraction
     * @param object, null
     *                for casting object to array during recursive call
     * @retun Object[] objects array for further processing
     */
    private Object[] buildArrayForCommonLogic(Object object, Field objectFiled) {
        return objectFiled != null ?
                (Object[]) ReflectionSupport.getObjectFromField(object, objectFiled) : (Object[]) object;
    }

    /**
     * This method calculate array element size based on next logic:
     * element is array - recurse call to common logic
     * element is not array - count it as link (basic logic)
     */
    private long getArrayElementSize(Object arrayElement) {
        return arrayElement.getClass().isArray() ? getSize(arrayElement, null) : 8;
    }
}
