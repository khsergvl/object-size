package com.millennium.falcon.impl.support;

import com.millennium.falcon.FieldSize;
import com.millennium.falcon.utils.ReflectionSupport;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class StringFieldSize implements FieldSize {

    @Override
    public long getSize(Object object, Field objectFiled) {
        long size = 32;
        String value = (String) ReflectionSupport.getObjectFromField(object, objectFiled);
        if (value != null) {
            size = size + value.length() * 2;
        }
        return size;
    }
}
