package com.millennium.falcon.impl.support;

import com.millennium.falcon.FieldSize;
import com.millennium.falcon.utils.ReflectionSupport;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class LongDoubleFieldSize implements FieldSize {

    @Override
    public long getSize(Object object, Field objectFiled) {
        long size = 8;
        Object value = ReflectionSupport.getObjectFromField(object, objectFiled);
        if (value != null) {
            size = size + 8;
        }
        return size;
    }
}
