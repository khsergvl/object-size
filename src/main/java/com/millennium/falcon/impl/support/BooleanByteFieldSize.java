package com.millennium.falcon.impl.support;

import com.millennium.falcon.FieldSize;

import java.lang.reflect.Field;

public class BooleanByteFieldSize implements FieldSize {

    @Override
    public long getSize(Object object, Field objectFiled) {
        return 1;
    }
}
