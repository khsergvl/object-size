package com.millennium.falcon.impl;

import com.millennium.falcon.ObjectSize;
import com.millennium.falcon.FieldSize;
import com.millennium.falcon.impl.support.ArrayFieldSize;
import com.millennium.falcon.impl.support.BooleanByteFieldSize;
import com.millennium.falcon.impl.support.CharShortFieldSize;
import com.millennium.falcon.impl.support.DateFieldSize;
import com.millennium.falcon.impl.support.IntFloatFieldSize;
import com.millennium.falcon.impl.support.LongDoubleFieldSize;
import com.millennium.falcon.impl.support.StringFieldSize;
import com.millennium.falcon.utils.ReflectionSupport;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class ObjectSizeImpl implements ObjectSize {

    private static ObjectSize singletonInstance;

    private Map<Class, FieldSize> context;

    private ObjectSizeImpl() {
    }

    private ObjectSizeImpl(Map<Class, FieldSize> context) {
        this.context = context;
    }

    /**
     * One of the way how to build singleton instance
     */
    public static synchronized ObjectSize getInstance() {
        if (singletonInstance == null) {
            Map<Class, FieldSize> context = new HashMap<>();
            context.put(boolean.class, new BooleanByteFieldSize());
            context.put(byte.class, new BooleanByteFieldSize());
            context.put(char.class, new CharShortFieldSize());
            context.put(short.class, new CharShortFieldSize());
            context.put(int.class, new IntFloatFieldSize());
            context.put(float.class, new IntFloatFieldSize());
            context.put(Long.class, new LongDoubleFieldSize());
            context.put(Double.class, new LongDoubleFieldSize());
            context.put(String.class, new StringFieldSize());
            context.put(Date.class, new DateFieldSize());
            context.put(Array.class, new ArrayFieldSize());
            singletonInstance = new ObjectSizeImpl(context);
        }
        return singletonInstance;
    }

    @Override
    public long getSize(Object object) {
        long size = 8;
        if (object != null) {
            List<Field> objectFields = getDeclaredFields(object);
            for (Field objectFiled : objectFields) {
                size = size + getSizeForCommonTypes(object, objectFiled);
            }
        }
        return size;
    }

    private List<Field> getDeclaredFields(Object object) {
        Field[] allFields = object.getClass().getDeclaredFields();
        return Arrays.asList(allFields);
    }

    @Override
    public long getSize(Object object, boolean deep) {
        if (deep) {
            long initSize = 8;
            return deepSize(object, initSize);
        } else {
            return getSize(object);
        }
    }

    private long deepSize(Object object, long size) {
        if (object != null) {
            List<Field> objectFields = getDeclaredFields(object);
            for (Field objectFiled : objectFields) {
                long filedSize = getSizeForCommonTypes(object, objectFiled);
                if (!isSameObjectOrEmptyReference(object, objectFiled) && (filedSize == 0)) {
                    Object value = ReflectionSupport.getObjectFromField(object, objectFiled);
                    filedSize = getSize(value, true);
                }
                //Empty reference
                if (filedSize == 0) {
                    filedSize = 8;
                }
                size = size + filedSize;
            }
        }
        return size;
    }

    private long getSizeForCommonTypes(Object object, Field objectFiled) {
        FieldSize fieldSizeCalculator = context.get(objectFiled.getType());
        if (fieldSizeCalculator != null) {
            return fieldSizeCalculator.getSize(object, objectFiled);
        }
        if (objectFiled.getType().isArray()) {
            fieldSizeCalculator = context.get(Array.class);
            return fieldSizeCalculator.getSize(object, objectFiled);
        }
        return 0;
    }

    private boolean isSameObjectOrEmptyReference(Object object, Field objectFiled) {
        boolean isSameOrEmptyReference = false;
        Object value = ReflectionSupport.getObjectFromField(object, objectFiled);
        if ((value != null) && (System.identityHashCode(object) == System.identityHashCode(value))) {
            return true;
        }
        if ((value == null)) {
            return true;
        }
        return isSameOrEmptyReference;
    }
}
