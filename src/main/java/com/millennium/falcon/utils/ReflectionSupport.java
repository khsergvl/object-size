package com.millennium.falcon.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class ReflectionSupport {
    private ReflectionSupport() {

    }

    public static Object getObjectFromField(Object object, Field objectFiled) {
        Object value = null;
        try {
            objectFiled.setAccessible(true);
            value = objectFiled.get(object);
        } catch (Exception e) {
            log.warn("Unreachable field");
        }
        return value;
    }

}
