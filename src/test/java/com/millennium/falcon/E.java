package com.millennium.falcon;

import java.util.Date;

public class E {                    // 8 bytes header + padding
    private int a;            // 4 bytes
    private Long c;            // 16 bytes (8 reference + 8 long)
    private String b;        // 32 bytes + 2 * length (header and caches + n-chars)
    private Object arr[];        // 12 bytes + 8 * length (header + n-references)
    private Date e;            // 8 bytes (reference to Date object)
    private E anotherE = this;
    private D d = new D();
}
