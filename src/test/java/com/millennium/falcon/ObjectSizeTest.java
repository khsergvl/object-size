package com.millennium.falcon;

import com.millennium.falcon.impl.ObjectSizeImpl;
import org.junit.Assert;
import org.junit.Test;

public class ObjectSizeTest {

    @Test
    public void simpleClassWithEmptyFieldsTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(72, objectSize.getSize(new A()));
        Assert.assertEquals(72, objectSize.getSize(new A(), false));
    }

    @Test
    public void simpleClassWithNonEmptyFieldsTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(96, objectSize.getSize(new B()));
        Assert.assertEquals(96, objectSize.getSize(new B(), false));
    }

    @Test
    public void deepClassWithEmptyFieldsTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(96, objectSize.getSize(new C(), true));
    }

    @Test
    public void deepClassWithNonEmptyFieldsTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(80, objectSize.getSize(new D(), true));
    }

    @Test
    public void deepClassWithNonEmptyAndInheritanceFieldsTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(160, objectSize.getSize(new E(), true));
    }

    @Test
    public void multidimensionalArrayFieldTest() {
        ObjectSize objectSize = ObjectSizeImpl.getInstance();
        Assert.assertEquals(156, objectSize.getSize(new F()));
        Assert.assertEquals(164, objectSize.getSize(new F(), true));
    }

}